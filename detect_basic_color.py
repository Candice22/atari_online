import time
import sys
import zmq
import json
from ale_python_interface import ALEInterface
from random import randrange
import zerorpc
import copy

init_frame = 150
def send_command():
    real_world=['','']
    c = zerorpc.Client()
    c.connect("tcp://127.0.0.1:4242")
    frame = 0
    legal_actions = ale.getLegalActionSet()
    
    f = open('./real_world.txt','w+')
    f.write('')
    f.close()
    f = open('./real_world.txt','a+')
    skip_index = 0
    i =-3
    existed = []
    while not ale.game_over():
        action = legal_actions[randrange(len(legal_actions))]
        if action == 'EAGAIN':
            break
        reward = ale.act(action)
        screen = ale.getScreenGrayscale()
        for i in xrange(45,60):
            f.write(str(i))
            for j in xrange(1,160):
                if screen[i][j][0] != 32:
                    if screen[i][j][0] not in existed:
                        existed.append(screen[i][j][0])
                        print i,j,screen[i][j][0]
                else:
                    f.write(' ')
            f.write('\n')
        #image = ale.saveScreenPNG(str(frame))
        lives = ale.lives()
        if (ale.getFrameNumber() >init_frame):
            frame+=1
            output = {'screen':screen,'reward':reward,'lives':lives,'action':action}
            result = get_real_world(output,frame)
            if(len(result)!=0):
                i+=1
                result = 't('+str(i)+')'+ str(result)
                real_world = [real_world[1],result]
                if(i>-1):    
                    rep={}
                    rep[i]=real_world
        skip_index +=1
def get_real_world(output,frame):
    return ''

if __name__ == '__main__':
    
    ale = ALEInterface()
    ale.setBool('display_screen', True)
    ale.loadROM('./ROMS/seaquest.bin')
    send_command()
