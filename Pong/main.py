import time
import cPickle as pickle

def pong(output,frame):
    colors={'L_B':[129,148],'R_B':[133,147],'Ball':[74,236]}
    contact =[]
    border ={'L_B':[],'R_B':[],'Ball':[]}
    pixel ={'L_B':[],'R_B':[],'Ball':[]}
    for i in xrange(34,194):
        for j in xrange(0,160):
            mark = 0
            for key,val in colors.items():
                for color in val:
                    if color == output['screen'][i][j][0]:
                        pixel[key].append([i,j])
                        break
                if mark:
                    break
    
    for key,val in pixel.items():
        if val!=[]:
            border[key] = [val[0][0],val[-1][0],val[0][1],val[-1][1]]
    
    if border['R_B']!=[]:
        for i in xrange(border['R_B'][0],border['R_B'][1]+1):
            if output['screen'][i][border['R_B'][2]-1][0] == 74 or output['screen'][i][border['R_B'][2]-1][0] == 236:
                contact.append({'R_B':[i,border['R_B'][2]-1]})
            if output['screen'][i][border['R_B'][3]+1][0] == 74 or output['screen'][i][border['R_B'][3]+1][0] ==236:
                contact.append({'R_B':[i,border['R_B'][3]+1]})               
        for j in xrange(border['R_B'][2],border['R_B'][3]+1):
            if border['R_B'][0]!=34 and (output['screen'][border['R_B'][0]-1][j][0] == 74 or output['screen'][border['R_B'][0]-1][j][0] ==236):
                contact.append({'R_B':[border['R_B'][0]-1,j]})
            if border['R_B'][1]!=193 and (output['screen'][border['R_B'][1]+1][j][0] == 74 or output['screen'][border['R_B'][1]+1][j][0] ==236):
                contact.append({'R_B':[border['R_B'][1]+1,j]})
    if border['L_B']!=[]:
        for i in xrange(border['L_B'][0],border['L_B'][1]+1):
            if output['screen'][i][border['L_B'][2]-1][0] == 74 or output['screen'][i][border['L_B'][2]-1][0] ==236:
                contact.append({'L_B':[i,border['L_B'][2]-1]})
            if output['screen'][i][border['L_B'][3]+1][0] == 74 or output['screen'][i][border['L_B'][3]+1][0] ==236:
                contact.append({'L_B':[i,border['L_B'][3]+1]})               
        for j in xrange(border['L_B'][2],border['L_B'][3]+1):
            if border['L_B'][0]!=34 and (output['screen'][border['L_B'][0]-1][j][0] == 74 or output['screen'][border['L_B'][0]-1][j][0] ==236):
                contact.append({'L_B':[border['L_B'][0]-1,j]})
            if border['L_B'][1]!=193 and (output['screen'][border['L_B'][1]+1][j][0] == 74 or output['screen'][border['L_B'][1]+1][j][0] ==236):
                contact.append({'L_B':[border['L_B'][1]+1,j]})
    if border['Ball']!=[]:
        if border['Ball'][0] == 34:
            for i in pixel['Ball']:
                if i[0]==34:
                    contact.append({'Top_shield':i})
        if border['Ball'][1] == 193:
            for i in pixel['Ball']:
                if i[0]==193:
                    contact.append({'bottom_shield':i})
        if border['Ball'][2] == 3:
            for i in pixel['Ball']:
                if i[1]==3:
                    contact.append({'L_wall':i})
        if border['Ball'][3] == 156:
            for i in pixel['Ball']:
                if i[1]==156:
                    contact.append({'R_wall':i})
    event ='t('+str(frame)+')'
    for key,val in border.items():
        if val!=[]:
            if key == "Ball":
                event+=', '+str(key)+'(id(0)),'+'loc('+str(border[key])+'),'+'lives('+str(output['lives'])+'))'
            else:
                event+=', '+str(key)+'(id(0)),'+'loc('+str(border[key])+'))'

    if contact!=[]:
        event+=', '+'contact('+str(contact)+')'
    event+=', '+ 'SCORE('+str(output['reward'])+ ')\n'
    print event
    return event


        
