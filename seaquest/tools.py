import cPickle as pickle
import copy
row_limit = [45,153]    #[0,210] by default
col_limit = [0,160]     #[0,160] by default

# filter out fake object by specific infor(sepcial for different game)
size_limit ={
    'player':{'row':[3,100],'col':[0,100]},
    'swimmer':{'row':[4,100],'col':[3,100]},
    'enemy':{'row':[5,100],'col':[3,100]},
    'bullet_p':{'row':[1,4],'col':[1,100]},
    'bullet':{'row':[1,3],'col':[1,100]}
    }

def get_row_range(size_limit,target_color,screen):
    cur=[]
    res=[]
    for i in xrange(row_limit[0],row_limit[1]):
        label = 0
        for j in xrange(160):
            if screen[i][j][0] in target_color:
                cur.append(i)
                label = 1
                break          
        if (cur!=[] and label==0) or (cur!=[] and i==row_limit[1]-1):
            if cur[-1]-cur[0]>=size_limit[0] and cur[-1]-cur[0]<=size_limit[1]:
                res.append(cur)
            cur = []
    return res


def get_border_range(size_limit,res,target_color,screen):#rectangle border
    cur=[]
    rec_shape=[]     #[start_row,end_row,start_col,end_col]
    for x in res:
        for j in xrange(col_limit[0],col_limit[1]):
            label = 1
            for i in xrange(x[0] ,x[-1]+1):
                if screen[i][j][0] in target_color:
                    cur.append(j)
                    label = 0
                    break
            if (cur!=[] and label) or (cur!=[] and i==col_limit[1]-1):
                if cur[-1]-cur[0]>=size_limit[0] and cur[-1]-cur[0]<=size_limit[1]:
                    rec_shape.append([x[0],x[-1],cur[0],cur[-1]]) 
                cur = []
    return rec_shape

#Recognize border of different objects
def get_all_borders(colors,border,screen):
    for key,val in colors.items():
        s = get_border_range(size_limit[key]['col'],get_row_range(size_limit[key]['row'],val,screen),val,screen)
        border[key]=s
        
    print 'bullet_p',border['bullet_p'],'player',border['player']
    return border



#find all pixels of objects
def get_pixels(colors,border,pixel,screen):

    for key,val in border.items():
        for l in val:
            cur=[]
            if l!=[]:
                for row in xrange(l[0],l[1]+1):
                    cur_row = []
                    for col in xrange(l[2],l[3]+1):
                        if(screen[row][col][0] in colors[key]):
                            cur_row.append([row,col])
                if len(cur_row)>0:
                    cur.append(cur_row)
            pixel[key].append(cur)
    return pixel


#find contact between any two different objects
def find_contact(entity_pixel, entity, colors,screen,background_color):

    # assuming any entity only touch one another entity at one time
    #first row
    if entity_pixel[0][0] > 1:        
        for col in entity_pixel[0]:
            if screen[col[0]-1][col[1]] not in background_color:
                for key,val in colors.items():
                    if screen[col[0]-1][col[1]] in val:
                        return {entity:[col[0],col[1]], key:[col[0]-1,col[1]]}
    #last row
    if entity_pixel[len(entity_pixel)-1][0] <159:
        for col in entity_pixel[len(entity_pixel)-1]:
            if screen[col[0]+1][col[1]] not in background_color:
                for key,val in colors.items():
                    if screen[col[0]+1][col[1]] in val:
                        return {entity:[col[0],col[1]], key:[col[0]+1,col[1]]}
    #first col and last col
    for row in entity_pixel:
        if row[0][1] > 1: 
            if screen[row[0][0]][row[0][1]-1] not in  background_color:
                for key,val in colors.items():
                    if screen[row[0][0]][row[0][1]-1] in val:
                        return {entity:[row[0][0],row[0][1]],key:[row[0][0],row[0][1]-1]}
        if row[len(row)-1][1] <159: 
            if screen[row[len(row)-1][0]][row[len(row)-1][1]+1] not in  background_color:
                for key,val in colors.items():
                    if screen[row[len(row)-1][0]][row[len(row)-1][1]+1] in val:
                        return {entity:[row[len(row)-1][0],row[len(row)-1][1]],key:[row[len(row)-1][0],row[len(row)-1][1]+1]} 
    return {}

def get_rel_distance(entity_a,entity_b,loc_a,borders_b,borders_b_pre):
    # relaive_distance based on bounding box
    # detect moving direction of entity_b, the rd we want is in the moving direction
    rd_vertical = -1
    rd_horizontal = -1
    res = []
    for i in xrange(len(borders_b_pre)):
        if (i<len(borders_b)):
            if (borders_b_pre[0]-borders_b[0])*(borders_b_pre[1]-borders_b[1])>0:
                if borders_b_pre[0]-borders_b[0]>0:
                    #entity_b moving up
                    for col in xrange(loc_a[2],loc_a[3]+1):
                        for row in xrange(loc_a[1]+1,210):
                            if(row in xrange(borders_b[0],borders_b[1]+1) and col in xrange(borders_b[2],borders_b[3]+1)):
                                rd_vertical = row - loc_a[1] +1
                else:
                    #entity_b moving down
                    for col in xrange(loc_a[2],loc_a[3]+1):
                        for row in xrange(0,loc_a[0]+1):
                            if(row in xrange(borders_b[0],borders_b[1]+1) and col in xrange(borders_b[2],borders_b[3]+1)):
                                rd_horizontal = loc_a[0]-row +1

            if (borders_b_pre[2]-borders_b[2])*(borders_b_pre[3]-borders_b[3])>0:
                if borders_b_pre[2]-borders_b[2]>0:
                    #entity_b moving left
                    for row in xrange(loc_a[0],loc_a[1]+1):
                        for col in xrange(loc_a[3]+1,160):
                            if(row in xrange(borders_b[0],borders_b[1]+1) and col in xrange(borders_b[2],borders_b[3]+1)):
                                rd_horizontal = col - loc_a[3] +1

                else:
                    #entity_b moving right
                    for row in xrange(loc_a[0],loc_a[1]+1):
                        for col in xrange(0,loc_a[2]):
                            if(row in xrange(borders_b[0],borders_b[1]+1) and col in xrange(borders_b[2],borders_b[3]+1)):
                                rd_horizontal = loc_a[2]-col +1

        if(rd_vertical>=0):
            res.append(rd_vertical)
        if(rd_horizontal>=0):
            res.append(rd_horizontal)
        else:
            res.append(-1)
    return res

#range of A contains B

def is_a_contain_b(A,B):
    
    if (B==[] and A!=[]) or (A==[] and B==[]):
        return True
    elif B!=[] and A==[]:
        return False
    elif B[0]>=A[0] and B[1]<=A[1] and B[2]>=A[2] and B[3]<=A[3]:
        return True
    else:
        return False           
                             
                
       
        

            
            
        
    
