import time
import sys
import zmq
import json
import cPickle as pickle
from ale_python_interface import ALEInterface
import zerorpc
import copy
from space_invader.get_pixel import border_range,border_pixels,border_all,B_pixel,P_pixel,P_border,RE_border,RE_pixel
from space_invader.contact import find_contact
import cv2
from random import randrange


background_color = [0,60,32]
entity_colors = {
    'player': [172,217],                        #53,236,117, #[60,32]
    'swimmer': [85,64,144,141],
    'enemy': [127,147,141,157,197,252],         #when overlap with swimmer, color is 141 , animating color is 157,197
    #'bullet_p': [172,217,144],                  #when overlap with swimmer,color is 144
    #'bullet':[85]
    }
colors_all = {
    'player': [172,217],
    'swimmer': [85,64,144,141],
    'enemy': [127,147,141,157,197,252],
    'bullet_p': [172,217,144],
    'bullet':[85]
    }
init_frame = 200
#top_left_point_list
tl_point_list =[]
entity_name_list =[]






def send_command():
    real_world=['','']
    c = zerorpc.Client()
    c.connect("tcp://127.0.0.1:4242")
    frame = 0
    legal_actions = ale.getLegalActionSet()
    
    f = open('./real_world','w+')
    f.write('')
    f.close()
    f = open('./real_world','a+')
    skip_index = 0
    i =-3
    
    while not ale.game_over():
        action = legal_actions[randrange(len(legal_actions))]
        #action = '0'
        if action == 'EAGAIN':
            break
        reward = ale.act(action)
        screen = ale.getScreenGrayscale()
        #image = ale.saveScreenPNG(str(frame))
        lives = ale.lives()
        if (ale.getFrameNumber() >init_frame and skip_index%2==0):
            frame+=1
            output = {'screen':screen,'reward':reward,'lives':lives,'action':action}
            result = get_atari_visualization(output,frame)
            if(len(result)!=0):
                i+=1
                result = 't('+str(i)+')'+ str(result)
                real_world = [real_world[1],result]
                if(i>-1):    
                    rep={}
                    rep[i]=real_world
                    f.write(result)
        skip_index +=1

def get_atari_visualization(output,frame):
    
    #tracker = cv2.Tracker_create("MIL")
    global tl_point_list,unique_size_list,entity_name_list
    contact = {}

    img_score = output['screen'][167:190]
    ret_score, binary_score = cv2.threshold(img_score,80,255,cv2.THRESH_BINARY)
    contours_score, hierarchy_score = cv2.findContours(binary_score, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(binary_score, contours_score, -1,(128,255,255), 0)
    cv2.imshow("img_score", binary_score)
    cv2.waitKey(10)
    for i in contours_score:
        x,y,w,h=cv2.boundingRect(i)
        if x==48:
            oxgen = w/65.0
            break

    #set manually
    img = output['screen'][45:153]
    ret, binary = cv2.threshold(img,60,255,cv2.THRESH_BINARY)
    #img_blurred = cv2.GaussianBlur(img,(5,5),0)
    #binary = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
    contours, hierarchy = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    debug= []
    if frame==1:
        still_existed_entity =[]
        debug = []
        for i in contours:
            x,y,w,h=cv2.boundingRect(i)
            cv2.rectangle(img,(x,y),(x+w,y+h),(128,255,255),0)
            if w>4:            
                #make coordiate consistent with original image because of cutting part of image previously
                #y +=45
                M = cv2.moments(i)
                # compute the center of the contour
                if M["m00"] != 0:
                    cX = int(M["m10"] / M["m00"])
                    cY = int(M["m01"] / M["m00"])
                    for entity_type in entity_colors:
                        if img[cY][cX][0] in entity_colors[entity_type]:
                            if entity_type == 'player' and w<10:
                                entity_name_list.append('bullet_p')
                                tl_point_list.append([y,y+h,x,x+w])
                                break
                            entity_name_list.append(entity_type)
                            tl_point_list.append([y,y+h,x,x+w])         
                            break
        entity_no_last_frame =0
            
    else:
        #update location
        for k in xrange(len(tl_point_list)):
            m = tl_point_list[k]
            new = []
            if m!=[]:
                colors = colors_all[entity_name_list[k]]
                # update row xrange
                cur = []
                for i in xrange(max(0,m[0]-5),min(m[1]+6,108)):
                    for j in xrange(max(0,m[2]-5),min(m[3]+6,160)):
                        if img[i][j][0] in colors:
                            cur.append(i)
                            break
                if cur!=[]:
                    new.append(cur[0])
                    new.append(cur[-1])
                # update column xrange
                    cur = []
                    for i in xrange(max(0,m[2]-5),min(m[3]+6,160)):
                        for j in xrange(new[0],new[-1]+1):
                            if img[j][i] in colors:
                                cur.append(i)
                                break
                    new.append(cur[0])
                    new.append(cur[-1])
            if len(new)==0:
                entity_name_list[k] = []
            tl_point_list[k] = new
        still_existed_entity =[]
        entity_no_last_frame = len(tl_point_list)

        #detect contact:
        for k in xrange(len(tl_point_list)):
            if tl_point_list[k]!=[] and (k not in contact):
                contact_entity = get_contact(tl_point_list[k],k,img,tl_point_list,background_color)
                if contact_entity != -1:
                    contact[contact_entity] = k
                    print frame,entity_name_list[k],entity_name_list[contact_entity]
                    
        for i in contours:
            x,y,w,h = cv2.boundingRect(i)
            cv2.rectangle(img,(x,y),(x+w,y+h),(128,255,255),0)
            debug.append([x,y,w,h])

            matched_id = get_matched_id_in_last_frame(tl_point_list,[y,y+h,x,x+w])
            #print x,y,w,h,matched_id
            #print 'tl_point_list',tl_point_list
            '''
            if w==5 and h==1:
                tl_point_list.append([x,y,w,h])
                entity_name_list.append('bullet')
                continue
            '''
            if matched_id == -1:
                if w>4:
                    M = cv2.moments(i)
                    if M["m00"] != 0:
                            cX = int(M["m10"] / M["m00"])
                            cY = int(M["m01"] / M["m00"])
                            for entity_type in entity_colors:
                                if img[cY][cX] in entity_colors[entity_type]:
                                    # set manually
                                    if entity_type == 'player' :
                                        if w<10 and  h<4:
                                            entity_name_list.append('bullet_p')
                                            tl_point_list.append([y,y+h,x,x+w])
                                            break
                                        elif 'player' in entity_name_list:
                                            break
                                    entity_name_list.append(entity_type)
                                    tl_point_list.append([y,y+h,x,x+w])         
                                    break
                    else:
                        #h=1 ---M["m00"]= 0
                        if w==5:
                            tl_point_list.append([y,y+h,x,x+w])
                            entity_name_list.append('bullet')
                        else:
                            entity_name_list.append('bullet_p')
                            tl_point_list.append([y,y+h,x,x+w])  
                continue
            '''
            if 'no_overlap' in matched_id:
                if matched_id['no_overlap'] in still_existed_entity:
                    if tl_point_list[matched_id['no_overlap']][2]*tl_point_list[matched_id['no_overlap']][3]<w*h:
                         tl_point_list[matched_id['no_overlap']] = [x,y,w,h]
                else:
                    tl_point_list[matched_id['no_overlap']] = [x,y,w,h]
                    still_existed_entity.append(matched_id['no_overlap'])
            if 'overlap' in matched_id:
                for j in matched_id['overlap']:
                    if tl_point_list[j][2]*tl_point_list[j][3]<w*h:
                        tl_point_list[j] = [x,y,w,h]
                    still_existed_entity.append(j)
            '''
    cv2.drawContours(binary, contours, -1,(128,255,255), 0)
    cv2.imshow("img", binary)
    cv2.waitKey(10)
    #print 'debug',debug
    '''
    for i in xrange(len(tl_point_list)):
        if i <entity_no_last_frame:
            if i not in still_existed_entity:
                tl_point_list[i] = []
                entity_name_list[i] = []
    '''            
    #print entity_name_list
    return ''
#min-val is specific for different games
def get_matched_id_in_last_frame(ele_list,ele):
    min_val =sys.maxint
    res = -1
    res_overlap = []
    is_overlap = False
    for i in xrange(len(ele_list)):
        if len(ele_list[i])==4:
            dif = abs(ele_list[i][0]-ele[0]) + abs(ele_list[i][2]-ele[2])
            if dif<min_val:
                res  = i
                min_val =dif
            if is_a_cover_b(ele,ele_list[i]) or is_a_cover_b(ele_list[i],ele):
                is_overlap = True
                res_overlap.append(i)

    if min_val > 15:
        if is_overlap:
            return {'overlap':res_overlap}
        else:
            return -1
    if res_overlap!=-1 and res!= res_overlap:
        return {'no_overlap':res,'overlap':res_overlap}
            
    return {'no_overlap':res}


def is_a_cover_b(a,b):
    #a,b[x,y,w,h]
    if (b[0] in xrange(a[0],a[1]+1)) and (b[2] in xrange(a[2],a[3]+1)):
        return True
    else:
        return False
# contact between any two different object        
def get_contact(bounding_box,entity_id,img,entity_list,background_color):
    # assuming any entity only touch one another entity at one time
    y1 = bounding_box[0]
    y2 = bounding_box[1]
    x1 = bounding_box[2]
    x2 = bounding_box[3]
    #first row
    if y1 > 1:        
        for col in xrange(x1,x2+1):
            if img[y1-1][col][0] not in background_color:
                for k in xrange(len(entity_list)):
                    if entity_list[k]==[] or k==entity_id:
                        continue
                    if y1-1 in xrange(entity_list[k][0],entity_list[k][1]+1) and col in xrange(entity_list[k][2],entity_list[k][3]+1):
                        return k
                    
    #last row
    if y2<107:
        for col in xrange(x1,x2+1):
            if img[y2+1][col][0] not in background_color:
                for k in xrange(len(entity_list)):
                    if entity_list[k]==[] or k==entity_id:
                        continue
                    if y2+1 in xrange(entity_list[k][0],entity_list[k][1]+1) and col in xrange(entity_list[k][2],entity_list[k][3]+1):
                        return k
    #first col 
    if x1 > 1:        
        for row in xrange(y1,y2+1):
            if img[row][x1-1][0] not in background_color:
                for k in xrange(len(entity_list)):
                    if entity_list[k]==[] or k==entity_id:
                        continue
                    if row in xrange(entity_list[k][0],entity_list[k][1]+1) and x1-1 in xrange(entity_list[k][2],entity_list[k][3]+1):
                        return k
    #last col
    if x2 < 159:        
        for row in xrange(y1,y2+1):
            if img[row][x2+1][0] not in background_color:
                for k in xrange(len(entity_list)):
                    if entity_list[k]==[] or k==entity_id:
                        continue
                    if row in xrange(entity_list[k][0],entity_list[k][1]+1) and x2+1 in xrange(entity_list[k][2],entity_list[k][3]+1):
                        return k
    
    return -1
    


    
if __name__ == '__main__':
    
    ale = ALEInterface()
    ale.setBool('display_screen', True)
    #ale.loadROM('./ROMS/space_invaders.bin')
    ale.loadROM('./ROMS/seaquest.bin')
    send_command()


