from ale_python_interface import ALEInterface
import copy
import zerorpc
from chopper.get_pixel import border1,border_pixels,BULLET_P,border_pixels_BP
from chopper.contain import contain_AB
from chopper.contact import find_contact,find_contact1

hp=0
pre_infor = {}
#name = ['player','enemy_w','enemy_b','bullet_p','bullet','truck']
#colors={'player':[184,167],'enemy_w':[236,0],'enemy_b':[92,17],'bullet':[184,167],'truck':[0]} #basic color
name = ['player','enemy_w','enemy_b','bullet_p','bullet']
action_list= {
    '1':'fire',
    '2':'up',
    '3':'right',
    '4':'left',
    '5':'down'
    }
colors={'player':[184,167],'enemy_w':[236,0],'enemy_b':[92,17],'bullet':[184,167]}
colors_all=[184,167,236,0,92,17,167,0,131,107,217,150,199]     #all color of objects inside main part except bullet_p
bullet_agent=[]
init_frame = 100


def send_command():
    real_world=['','']
    c = zerorpc.Client()
    c.connect("tcp://127.0.0.1:4242")
    frame = 0
    action = '1'
    reward = ale.act(action)
    
    f = open('./real_world','w+')
    f.write('')
    f.close()
    f = open('./real_world','a+')
    skip_index = 0
    i =-3
    
    while not ale.game_over():          
        if action == 'EAGAIN':
            break
        if frame>100:
            action = '4'
        reward = ale.act(action)
        screen = ale.getScreenGrayscale()
        #image = ale.saveScreenPNG(str(frame))
        lives = ale.lives()
        if (ale.getFrameNumber() >init_frame):
            frame+=1
            output = {'screen':screen,'reward':reward,'lives':lives,'action':action}
            result = chopper_command(output,frame)
            if(len(result)!=0):
                i+=1
                result = 't('+str(i)+')'+ str(result)
                real_world = [real_world[1],result]
                if(i>-1):    
                    rep={}
                    rep[i]=real_world
                    f.write(result)
        skip_index +=1
def chopper_command(output,time):

    global pre_infor,hp
    contact=[]
    border ={}
    pixel ={}
    border_cur={}  #border_cur updated border based on last time, border_new use border1 function to find all object
    pixel_cur={}
    status={}
    for val in name:
        border[val]= []
        pixel[val]=[]
        border_cur[val] = []
        pixel_cur[val] =[]
        status[val] =[]
    
    
    hp += int(output['reward'])
    #give Id to each object
    if time == 1:
        border_cur = border1(colors,border,output['screen'])
        pixel_cur = border_pixels(colors,border,pixel,output['screen'])
        pixel_cur['bullet_p'] = bullet_p(output['screen'],colors_all)
        status_pre = status
        if pixel['bullet_p']!=[]:
            border['bullet_p'] =[[pixel['bullet_p'][0][0][0],pixel['bullet_p'][0][-1][0],pixel['bullet_p'][0][0][1],pixel['bullet_p'][0][-1][1]]] 
    else:
        status_pre = pre_infor['status']
        border_pre= pre_infor['border']
        #update border

        for key,val in border_pre.items():
            if key=='enemy_b':
                for m in val:
                    label=0
                    new=[]
                    if m!=[]:
                        cur=[]
                        for i in xrange(max(56,m[0]-5),min(m[1]+5+1,168)):
                            for j in xrange(max(8,m[2]-5),min(m[3]+5+1,160)):
                                for c in colors[key]:
                                    if output['screen'][i][j][0]==c:
                                        cur.append(i)
                        if cur!=[]:                
                            new.append(cur[0])
                            new.append(cur[-1])
                        #update column range
                        cur=[]
                        for i in xrange(max(8,m[2]-5),min(m[3]+5+1,160)):
                            for j in xrange(max(56,m[0]-5),min(m[1]+5+1,168)):
                                for c in colors[key]:
                                    if output['screen'][j][i][0]==c:
                                        cur.append(i)
                        if cur!=[]:
                            new.append(cur[0])
                            new.append(cur[-1])
                    border_cur[key].append(new)
            elif key=='enemy_w':
                for m in val:
                    new=[]
                    label=0
                    if m!=[]:
                        cur=[]
                        for i in xrange(max(56,m[0]-5),min(m[1]+5+1,168)):
                            for j in xrange(max(8,m[2]-5),min(m[3]+5+1,160)):
                                for c in colors[key]:
                                    if output['screen'][i][j][0]==c or output['screen'][i][j][0]==0:
                                        cur.append(i)
                        if cur!=[]:                
                            new.append(cur[0])
                            new.append(cur[-1])
                        #update column range
                        cur=[]
                        for i in xrange(max(8,m[2]-5),min(m[3]+5+1,160)):
                            for j in xrange(max(56,m[0]-5),min(m[1]+5+1,168)):
                                for c in colors[key]:
                                    if output['screen'][j][i][0]==c or output['screen'][j][i][0]==0:
                                        cur.append(i)
                        if cur!=[]:
                            new.append(cur[0])
                            new.append(cur[-1])
                    border_cur[key].append(new)
            elif key == 'bullet_p':
                for m in val:
                    new =[]
                    new1=[]
                    if m!=[]:
                        cur =[]
                        for i in xrange(m[-1],8,-1):
                                    if output['screen'][m[0]][i][0] not in colors_all:
                                        cur.append(i)
                                    elif output['screen'][m[0]][i][0]==131:
                                        break
                        if cur!=[]:                
                            new1.append(cur[-1])
                        cur=[]
                        for i in xrange(m[-1],160):
                                    if output['screen'][m[0]][i][0] not in colors_all:
                                        cur.append(i)
                                    elif output['screen'][m[0]][i][0]==131:
                                        break
            
                        if cur!=[]:                
                            new1.append(cur[-1])
                            new.append(m[0])
                            new.append(m[0])
                            new.extend(new1)
                    border_cur[key].append(new)
            elif key=='truck':
                for m in val:
                    new=[]
                    if m!=[]:              
                        #update column range
                        cur=[]
                        for i in xrange(max(8,m[2]-5),min(m[3]+5+1,160)):
                            for j in xrange(max(56,m[0]),min(m[1]+5+1,168)):
                                for c in colors[key]:
                                    if output['screen'][j][i][0]==c or output['screen'][j][i][0]==107:
                                        cur.append(i)
                        if cur!=[]:
                            new.append(m[0])
                            new.append(m[1])
                            new.append(cur[0])
                            new.append(cur[-1])
                    border_cur[key].append(new)
                    
            elif key=='bullet':
                for m in val:
                    new=[]
                    if m!=[]:
                        cur=[]
                        for i in xrange(max(56,m[0]-3),min(m[1]+4,168)):
                            label=1
                            for j in xrange(min(m[2]-3,168),min(m[3]+4,160)):
                                for c in colors[key]:
                                    if output['screen'][i][j][0]==c:
                                        cur.append(i)
                                        label=0
                                        break
                            if label and cur!=[]:
                                break
                        if cur!=[]:                
                            new.append(cur[0])
                            new.append(cur[-1])
                        #update column range
                        cur=[]
                        for i in xrange(max(8,m[2]-3),min(m[3]+4,160)):
                            label=1
                            for j in xrange(max(56,m[0]-3),min(m[1]+4,168)):
                                for c in colors[key]:
                                    if output['screen'][j][i][0]==c:
                                        cur.append(i)
                                        label=0
                                        break
                            if label and cur!=[]:
                                break                            
                        if cur!=[]:
                            new.append(cur[0])
                            new.append(cur[-1])
                    if new not in border_cur[key]:
                        border_cur[key].append(new)     
        for i in BULLET_P(output['screen'],colors_all):
            border['bullet_p'].append([i[0][0],i[0][0],i[0][1],i[-1][1]])
        border_new = border1(colors,border,output['screen'])
        border_cur['player'] = border_new['player']
        border_new['bullet_p']=border['bullet_p']
        #add new objects
        for key,val in border_new.items():
            if key=='bullet_p':
                for i in val:
                    label=1
                    for j in border_cur[key]:
                        if contain_AB(j,i):
                            label=0
                            break
                    if i not in border_cur[key] and label:
                            border_cur[key].append(i)
            elif key=='bullet':
                cur=[]
                for i in border_cur[key]:
                    if i==[]:
                        cur.append([])
                        continue
                    for j in val:
                        if contain_AB(j,i):
                                if j in cur:
                                    continue
                                cur.append(j)
                                break
                for j in border_new[key]:
                    if j in cur:
                        continue;
                    cur.append(j)        
                border_cur['bullet']=cur
                for i in xrange(len(border_cur[key])):
                    if i>len(bullet_agent)-1:
                        for j in xrange(len(border_cur['enemy_w'])):
                            if len(border_cur['enemy_w'][j])==4 and border_cur['bullet'][i][2]>=border_cur['enemy_w'][j][2] and border_cur['bullet'][i][2]<=border_cur['enemy_w'][j][3]:
                                bullet_agent.append(['enemy_w'+str(j+1)])
                                break
                            elif bullet_agent!=[]:
                                 bullet_agent.append(bullet_agent[-1])
                            continue
                        for j in xrange(len(border_cur['enemy_b'])):
                            if len(border_cur['enemy_b'][j])==4 and border_cur['bullet'][i][2]>=border_cur['enemy_b'][j][2] and border_cur['bullet'][i][2]<=border_cur['enemy_b'][j][3]:
                                bullet_agent.append(['enemy_b'+str(j+1)])
                                break
                            elif bullet_agent!=[]:
                                 bullet_agent.append(bullet_agent[-1])
                            continue
            elif key=='enemy_w':
                '''
                if len(border_new['enemy_w'])==2:
                    cur = [[min(border_new['enemy_w'][0][0],border_new['enemy_w'][1][0]),max(border_new['enemy_w'][0][1],border_new['enemy_w'][1][1]),min(border_new['enemy_w'][0][2],border_new['enemy_w'][1][2]),max(border_new['enemy_w'][0][3],border_new['enemy_w'][1][3])]]
                    border_new['enemy_w']=cur
                '''
                for i in border_new['enemy_w']:
                    label=1
                    for j in xrange(len(border_cur[key])):
                        if border_cur[key][j]==[]:
                            continue
                        elif contain_AB(border_cur[key][j],i):
                            label=0
                            break
                        elif contain_AB(i,border_cur[key][j]):
                            border_cur[key][j]=i
                            label=0
                            break
                    if label:
                       border_cur[key].append(i)        
            else:    
                for i in val:
                    label=1
                    for j in xrange(len(border_cur[key])):
                        if border_cur[key][j]==[]:
                            continue
                        elif contain_AB(border_cur[key][j],i):
                            label=0
                            break
                        elif contain_AB(i,border_cur[key][j]):
                            border_cur[key][j]=i
                            label=0
                            break
                    if label:
                       border_cur[key].append(i)
    pixel_cur = border_pixels(colors,border_cur,pixel,output['screen'])
    pixel_cur['bullet_p']=border_pixels_BP(colors_all,border_cur['bullet_p'],output['screen'])

    for m in name:
        for i in xrange(len(border_cur[m])):
            if border_cur[m][i]==[]:
                status[m].append(0)
            elif i < len(status_pre[m]) and status_pre[m][i]==-1:
                status[m].append(-1)
            else:
                status[m].append(1)

    event = ''
    for i in name:
        if i=='player':
            for j in xrange(len(border_cur[i])):
                if len(border_cur[i][j])>0:
                    event += ', '+i+'('+ 'id(p'+str(j+1)+'),gid(p'+str(j+1)+'),hp('+str(hp)+'),lives('+str(output['lives'])+'),loc_b('+str(border_cur[i][j][1])+'),loc_c('+str(border_cur[i][j][2])+'),status('+str(status[i][j])+'))'
    
        elif i=='bullet_p':
            for j in xrange(len(border_cur[i])):
                if len(border_cur[i][j])>0:
                    event += ', '+i+'('+ 'id(b'+str(j+1)+'),gid(p1),loc_b('+str(border_cur[i][j][1])+'),loc_c('+str(border_cur[i][j][2])+'))'

        elif i=='bullet':
            for j in xrange(len(border_cur[i])):
                if len(border_cur[i][j])>0 and j< len(bullet_agent): 
                    event += ', '+i+'('+ 'id(b'+str(j+1)+'),'+'gid('+str(bullet_agent[j][0])+'),'+'loc_b('+str(border_cur[i][j][1])+'),loc_c('+str(border_cur[i][j][2])+'))'
        else:
            for j in xrange(len(border_cur[i])):
                if len(border_cur[i][j])>0:
                    event += ', '+i+'('+ 'id(e'+i[-1]+str(j+1)+'),gid(e'+i[-1]+str(j+1)+'),loc_b('+str(border_cur[i][j][1])+'),loc_c('+str(border_cur[i][j][2])+'),status('+str(status[i][j])+'))'    
    # find contact point, exploding is detected based on contact
    for i in xrange(len(border_cur['bullet'])):
        for j in xrange(len(pixel_cur['player'])):
            if border_cur['bullet'][i]!=[]:
                s=find_contact(border_cur['bullet'][i],pixel_cur['player'][j],'bullet','player')
                if s!=[]:
                    status['player'][j]=-1
                    contact.append(s)
    for i in xrange(len(border_cur['bullet_p'])):
        for j in xrange(len(pixel_cur['enemy_w'])):
            if border_cur['bullet_p'][i]!=[]:
                s=find_contact1(border_cur['bullet_p'][i],pixel_cur['enemy_w'][j],'bullet_p','enemy_w')
                if s!=[]:
                    status['enemy_w'][j] = -1
                    contact.append(s)
        for j in xrange(len(pixel_cur['enemy_b'])):
            if border_cur['bullet_p'][i]!=[]:
                s=find_contact1(border_cur['bullet_p'][i],pixel_cur['enemy_b'][j],'bullet_p','enemy_b')
                if s!=[]:
                    status['enemy_b'][j] = -1
                    contact.append(s)
    for i in xrange(len(border_cur['player'])):
        for j in xrange(len(pixel_cur['enemy_b'])):
            if border_cur['player'][i]!=[]:
                s=find_contact(border_cur['player'][i],pixel_cur['enemy_b'][j],'player','enemy_b')
                if s!=[]:
                    status['enemy_b'][j] =-1
                    status['player'][i] = -1
                    contact.append(s)
        for j in xrange(len(pixel_cur['enemy_w'])):
            if border_cur['player'][i]!=[]:
                s=find_contact(border_cur['player'][i],pixel_cur['enemy_w'][j],'player','enemy_w')
                if s!=[]:
                    status['enemy_w'][j] =-1
                    status['player'][i] = -1
                    contact.append(s)

    if contact!=[]:
        for i in xrange(len(contact)):
            event += ', '+'con('+'id(c'+str(i)+'),gid(c'+str(i)+'))'
    if output['action'] in action_list:
        event += ',' + action_list[output['action']]+'(id('+ action_list[output['action']][0]+'),gid('+action_list[output['action']][0]+'))'
    event +='\n'
    pre_infor['status'] = status
    pre_infor['border'] = border_cur
    return event
            
if __name__ == '__main__':
    
    ale = ALEInterface()
    ale.setBool('display_screen', True)
    ale.loadROM('./ROMS/chopper_command.bin')
    send_command()
    




    
