import cPickle as pickle

def row_range(target_color,screen):
    cur=[]
    res=[]
    for i in xrange(20,195):
        label = 0
        for j in xrange(160):
            for m in target_color:
                if screen[i][j][0] == m:
                    cur.append(i)
                    label = 1
            if label==1:
                break           
        if (cur!=[] and label==0) or (cur!=[] and i==194):
            res.append(cur)
            cur = []
    return res


def border_range(res,target_color,screen):#rectangle border
    cur=[]
    rec_shape=[]     #[start_row,end_row,start_col,end_col]
    for x in res:
        start_row = x[0]
        end_row = x[-1]
        for j in xrange(160):
            label = 0
            for i in xrange(start_row ,end_row+1):
                for m in target_color:
                    if screen[i][j][0] == m:
                        cur.append(j)
                        label = 1
                if label==1:
                        break
            if (cur!=[]and(label==0)):
                rec_shape.append([start_row,end_row,cur[0],cur[-1]]) 
                cur = []
    return rec_shape

#Recognize border of different objects
def border_all(colors,border,screen):
    for key,val in colors.items():
        s = border_range(row_range(val,screen),val,screen)
        border[key]=s
    return border



#find all pixels of objects
def border_pixels(colors,border,pixel,screen):

    for key,val in border.items():
        for l in val:
            cur=[]
            if l!=[]:
                for row in xrange(l[0],l[1]+1):
                    for col in xrange(l[2],l[3]+1):
                        for i in colors[key]:
                            if(screen[row][col][0]==i):
                                cur.append([row,col])
            pixel[key].append(cur)
    return pixel



def B_pixel(screen):
    res=[]
    cur=[]
    overlap=0
    back_index=0
    mark=1
    m=0
    for j in xrange(160):
        for i in xrange(20,195):
            label=0  #mark of end
            if m:
                i-=m
            if screen[i][j][0]==58:
                if overlap and mark:
                    label=1
                else:
                    cur.append([i,j])
            elif screen[i][j][0]==217:
                back_index+=1
                overlap = 1
                cur.append([i,j])
            elif screen[i][j][0] == 0:
                label=1
            if (cur!=[] and label) or (cur!=[] and i==194):
                res.append(cur)
                cur=[]
                overlap=0
                if back_index:
                    m=back_index+1
                    back_index=0
                    mark=0 if mark==1 else 1
    return res

def P_border(target_color,screen):
    border=border_range(row_range(target_color,screen),target_color,screen)
    return border

def P_pixel(target_color,screen):
    border=border_range(row_range(target_color,screen),target_color,screen)
    pixel=[]
    for l in border:
            cur=[]
            if l!=[]:
                for row in xrange(l[0],l[1]+1):
                    for col in xrange(l[2],l[3]+1):
                        for i in target_color:
                            if(screen[row][col][0]==i):
                                cur.append([row,col])
            pixel.append(cur)
    return pixel

def RE_border(screen):
    cur=[]
    res=[]
    for i in xrange(0,20):
        label = 0
        for j in xrange(160):
            if screen[i][j][0] == 74:
                cur.append(i)
                label = 1         
        if (cur!=[] and label==0) or (cur!=[] and i==19):
            res.append(cur)
            cur = []
    l=border_range(res,target_color,screen)
    return l

def RE_pixel(screen):
    val=BP_border(screen)
    pixel=[]
    for l in val:
        cur=[]
        if l!=[]:
            for row in xrange(l[0],l[1]+1):
                for col in xrange(l[2],l[3]+1):
                    for i in colors[key]:
                        if(screen[row][col][0]==i):
                            cur.append([row,col])
        pixel.append(cur)
    return pixel
    
    

