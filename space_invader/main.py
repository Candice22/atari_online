import cPickle as pickle
import copy
from Get_pixel import border_range,border_pixels,border1,B_pixel,P_pixel,P_border,RE_border
from contact import find_contact
import time

actions = {'1':'Fire','4':'force_left','3':'force_right'}
colors={'ENEMY':[122,95],'SHIELD':[107],'PLAYER':[98],'BULLET':[58,217],'RE':[74]}
shape_module=pickle.load(open("./space_invader/shape_module.txt",'r'))
ids= {}
for i in range(1,37):
    ids[i] = 'e'+str(i)
for i in range(37,40):
    ids[i] = 's'+str(i-36)
for i in range(42,100):
    ids[i] = 'b'+str(i-41)
ids[40] = 'p1'
ids[41] = 're1'

def space_invaders(output,frame):
    border ={'ENEMY':[],'SHIELD':[],'PLAYER':[],'BULLET':[],'RE':[]}
    pixel ={'ENEMY':[],'SHIELD':[],'PLAYER':[],'BULLET':[],'RE':[]}
    status={}
    contact =[{},{},{},{},{}]
    DRS = {}

    if frame == 1:
        shape_border = border1(colors,border,output['screen'])
        shape_pixel = border_pixels(colors,shape_border,pixel,output['screen'])
        status_pre = {}
        contact_pre = {}
        for i in range(1,50):
            status_pre[i]=0
    else:
        filename1= './frames_text/'+str(frame-1)+'.txt'
        f = open(filename1,'rb')
        status_pre = pickle.load(f)
        shape_border_pre= pickle.load(f)
        contact_pre = pickle.load(f)
        shape_border={'ENEMY':[],'SHIELD':[],'PLAYER':[],'BULLET':[]}
        for key,val in shape_border_pre.items():
            if key =='ENEMY':
                for m in val:
                    #update row range
                    if m!=[]:
                        cur=[]
                        new=[]
                        if m[1]-m[0]>9:
                            for i in xrange(max(0,m[0]+10),min(m[0]+25,195)):
                                for j in xrange(max(0,m[2]-2),min(m[3]+2+1,160)):
                                    for c in colors[key]:
                                        if output['screen'][i][j][0]==c:
                                            cur.append(i)
                            if cur!=[]:                
                                new.append(cur[0])
                                new.append(cur[-1])

                            #update column range
                            cur=[]
                            for i in xrange(max(0,m[2]-2),min(m[3]+2+1,160)):
                                for j in xrange(max(0,m[0]+10),min(m[0]+20,195)):
                                    for c in colors[key]:
                                        if output['screen'][j][i][0]==c:
                                            cur.append(i)
                            if cur!=[]:
                                new.append(cur[0])
                                new.append(cur[-1])
                            shape_border[key].append(new)
                            
                        else:
                            for i in xrange(max(0,m[0]-2),min(m[1]+2+1,195)):
                                for j in xrange(max(0,m[2]-2),min(m[3]+2+1,160)):
                                    for c in colors[key]:
                                        if output['screen'][i][j][0]==c:
                                            cur.append(i)
                            if cur!=[]:                
                                new.append(cur[0])
                                new.append(cur[-1])

                            #update column range
                            cur=[]
                            for i in xrange(max(0,m[2]-2),min(m[3]+2+1,160)):
                                for j in xrange(max(0,m[0]-3),min(m[1]+3+1,195)):
                                    for c in colors[key]:
                                        if output['screen'][j][i][0]==c:
                                            cur.append(i)
                            if cur!=[]:
                                new.append(cur[0])
                                new.append(cur[-1])
                            shape_border[key].append(new)
                    else:
                        shape_border[key].append(m)
            elif key=='SHIELD' or key=='RE':
                for m in val:
                    new=[]
                    if m!=[]:
                        cur=[]
                        for i in xrange(max(0,m[0]-5),min(m[1]+5+1,194)):
                            for j in xrange(max(0,m[2]-5),min(m[3]+5+1,160)):
                                for c in colors[key]:
                                    if output['screen'][i][j][0]==c:
                                        cur.append(i)
                        if cur!=[]:                
                            new.append(cur[0])
                            new.append(cur[-1])

                        #update column range
                        cur=[]
                        for i in xrange(max(0,m[2]-5),min(m[3]+5+1,160)):
                            for j in xrange(max(0,m[0]-5),min(m[1]+5+1,194)):
                                for c in colors[key]:
                                    if output['screen'][j][i][0]==c:
                                        cur.append(i)
                        if cur!=[]:
                            new.append(cur[0])
                            new.append(cur[-1])
                    shape_border[key].append(new)

            elif key=='BULLET':
                for m in val:
                    new =[]
                    #update row range
                    if m!=[]:
                        cur =[]
                        for i in xrange(max(0,m[0]-10),min(m[1]+10+1,195)):
                            for j in xrange(m[2],m[2]+1):
                                for c in colors[key]:
                                    if output['screen'][i][j][0]==c:
                                        cur.append(i)
                        if cur!=[]:                
                            new.append(cur[0])
                            new.append(cur[-1])
                            new.append(m[-1])
                            new.append(m[-1])
                    shape_border[key].append(new)   
     #updated shape_pixel
        shape_pixel = border_pixels(colors,shape_border,pixel,output['screen'])
    # add new bullet into shape_pixel
    bullet_pixel = B_pixel(output['screen'])
    for A in bullet_pixel:
        label =1
        for j in range(0,len(shape_pixel['BULLET'])):
            B = shape_pixel['BULLET'][j]
            if A==B:
                label=0
                break
            if any([A==B[i:i+len(A)] for i in range(0,len(B)-len(A)+1)]):
                shape_pixel['BULLET'][j]=A
                shape_border['BULLET'][j]=[A[0][0],A[-1][0],A[0][1],A[-1][1]]
                label=0
                break
        if label:
            shape_pixel['BULLET'].append(A)
            shape_border['BULLET'].append([A[0][0],A[-1][0],A[0][1],A[-1][1]])
    status = {}
    for i in range(1,200):
        status[i]=0
    # add new player into shape_pixel
    shape_border['PLAYER'] =P_border(colors['PLAYER'],output['screen'])
    shape_pixel['PLAYER'] = P_pixel(colors['PLAYER'],output['screen'])
    shape_pixel_origin = copy.deepcopy(shape_pixel)

    #shape_generalize and recognition 
    ID=1
    for val1 in shape_pixel['ENEMY']:
        if val1!=[]:
            label=1
            start_x=val1[0][0]
            start_y=val1[0][1]
            for j in val1:
                j[0]-=start_x
                j[1]-=start_y
            for key,val2 in shape_module.items():
                if val1==val2:
                    status[ID] = key[2:]
                    label=0
                    break
                else:
                    status[ID] = -1 
        ID+=1
    if label:
        for i in xrange(1,ID):
            status[i]=1
    ID=37
    for val in shape_pixel['SHIELD']:
        if val !=[]:
            start_x=val[0][0]
            start_y=val[0][1]
            for j in val:
                j[0]-=start_x
                j[1]-=start_y
            if val==shape_module['ST']:
                    status[ID] = 10
            else:
                status[ID]=-1
            if(ID==39):           #eliminating  inteference having same color with sheilds
                break
        ID+=1
    ID=40
    for val in shape_pixel['PLAYER']:
        if val !=[]:
            start_x=val[0][0]
            start_y=val[0][1]
            for j in val:
                j[0]-=start_x
                j[1]-=start_y
            if contact_pre!=[{},{},{},{},{}]:
                for m in contact_pre:
                    if m!={}:
                        if m['obj2']==40:
                            status[ID]=-1
                            break    
            elif status_pre[ID]==-1: 
                status[ID]=-1
            else:
                status[ID]=10
    ID=41
    for val in shape_pixel['RE']:
        if val !=[]:
            start_x=val[0][0]
            start_y=val[0][1]
            for j in val:
                j[0]-=start_x
                j[1]-=start_y
            if contact_pre!=[{},{},{},{},{}]:
                for m in contact:
                    if m!={}:
                        if m['obj2']==41:
                            status[ID]=-1
                            break    
            elif status_pre[ID]==-1:
                status[ID]=-1
            else:
                status[ID]=10

    ID=42
    pre = []
    A1=[122,95,107]
    A2=[107,98]
    k=0
    
    ID=42
    pre = []
    A1=[122,95,107]
    A2=[107,98]
    k=0
    for val in shape_pixel['BULLET']:
        if val==[]:
            ID+=1
            continue
        elif val==pre:         
            continue
        else: 
            status[ID]=[len(val)]
            status[ID].append([val[0][0],val[-1][0],val[0][1]])
            column = val[0][1]
            if status_pre[ID]!=0:
                if len(status_pre[ID])==3:
                    status[ID].append(status_pre[ID][2])
                    if status_pre[ID][2] == 40:
                        location = find_contact(val,output['screen'],A1)
                        if 'up_down' in location:
                            loc = [location['up_down'][0]-1,location['up_down'][1]]
                            for m in range(37):
                                if loc in shape_pixel_origin['ENEMY'][m-1]:
                                    contact[k]['obj1']=ID
                                    contact[k]['obj2']=m
                                    contact[k]['loc']=loc
                                    k+=1
                                    break
                            for m in range(37,39):
                                if loc in shape_pixel_origin['SHIELD'][m-37]:
                                    contact[k]['obj1']=ID
                                    contact[k]['obj2']=m
                                    contact[k]['loc']=loc
                                    k+=1
                                    break
                            if shape_pixel_origin['RE']!=[] and loc in shape_pixel_origin['RE'][0]:
                                contact[k]['obj1']=ID
                                contact[k]['obj2']=41
                                contact[k]['loc']=loc
                                k+=1
                    else:
                        location = find_contact(val,output['screen'],A2)
                        if 'down_up' in location:
                            loc = [location['down_up'][0]+1,location['down_up'][1]]
                            if shape_pixel_origin['PLAYER']!=[] and loc in shape_pixel_origin['PLAYER'][0]:
                                contact[k]['obj1']=ID
                                contact[k]['obj2']=40
                                contact[k]['loc']=loc
                                k+=1
                            for m in range(37,39):
                                if loc in shape_pixel_origin['SHIELD'][m-37]:
                                    contact[k]['obj1']=ID
                                    contact[k]['obj2']=m
                                    contact[k]['loc']=loc
                                    k+=1
                                    break
                
                elif status_pre[ID][1][0]< val[0][0]:
                    for i in xrange(35,-1,-1):
                         if shape_border['ENEMY'][i]!=[] and val[0][1]>=shape_border['ENEMY'][i][2] and val[0][1]<=shape_border['ENEMY'][i][3]:
                            if status[i+1]!=-1:
                                status[ID].append(i+1)
                                break
                            else:
                                i-=6      
                elif status_pre[ID][1][0] > val[0][0]:
                    status[ID].append(40)
                #add DRS
            mark = 1
            if len(status[ID])==3:
                if status[ID][2] == 40:
                    for row in range(val[0][0],20,-1):
                        if output['screen'][row][column][0]!=0:
                            DRS[ID] =val[0][0]-row -1
                            mark = 0
                            break
                    if mark:
                        DRS[ID]=-1
                else:
                    for row in range(val[-1][0]+1,195):
                        if output['screen'][row][column][0]!=0:
                            DRS[ID] = row - val[-1][0]-1
                            mark = 0
                            break
                    if mark:
                        DRS[ID]=-1
                        
            ID+=1
            pre=val
    
    filename2= './frames_text/'+str(frame)+'.txt'
    f = open(filename2,'wb')
    pickle.dump(status,f)
    pickle.dump(shape_border,f)
    pickle.dump(contact,f)

    '''
    event='t('+str(frame)+')'
    for i in range(1,37):
        if status[i]!=0:
            event+=', '+'E'+'(id('+str(i)+'),'+'status('+str(status[i])+'),'+'loc('+str(shape_border['ENEMY'][i-1])+'))'
        else:
            event+=', '+'E'+'(id('+str(i)+'),'+'status('+str(status[i])+'),'+'))'
    for i in range(37,40):
        if status[i]!=0:
            event+=', '+'S'+'(id('+str(i)+'),'+'status('+str(status[i])+'),'+'loc('+str(shape_border['SHIELD'][i-37])+'))'
        else:
            event+=', '+'S'+'(id('+str(i)+'),'+'status('+str(status[i])+')), '
    if status[40]!=0:
        event+=', '+ 'P'+'(id('+str(40)+'),'+'action('+str(output['action'])+'),'+'score('+str(output['reward'])+'),'+'LIVES('+str(output['lives'])+'),'+'status('+str(status[40])+'),'+'loc('+str(shape_border['PLAYER'][0])+'))'

    else:
        event+=', '+ 'P'+'(id('+str(40)+'),'+'status('+str(status[40])+'))'
    if status[41]!=0:
        event+=', '+ 'RE'+'(id('+str(41)+'),'+'status('+str(status[41])+'),'+'loc('+str(shape_border['RE'][0])+'))'
    else:
        event+=', '+ 'RE'+'(id('+str(41)+'),'+'status('+str(status[41])+'))'
    for i in range(42,200):
        if status[i]!=0:
            event+=', '+ 'B'+'(id('+str(i)+'),'+'status('+str(status[i])+'),'+'loc('+str(shape_border['BULLET'][i-42])+'))'
    if contact!=[{},{},{},{},{}]:
        for val in contact:
            if val!={}:
                event+=', '+'Con'+str(k)+'(id('+'c'+'),'+'obj1('+str(val['obj1'])+'),'+'obj2('+str(val['obj2'])+'),'+ 'Loc('+str(val['Loc'])+'))'

    event+=')\n'
    return event

    with open('/media/sf_common_file/Atari_online/record/input_1.txt','a+')as f:
        f.write('t('+str(frame)+')')
        for i in range(31,32):
            if status[i]!=0 and len(shape_border['ENEMY'][i-1])==4:
                f.write(', Enemy(id('+ids[i]+'),'+'gid('+ids[i]+'),'+'stat('+str(status[i])+'),loc_a('+str(shape_border['ENEMY'][i-1][0])+'),loc_b('+str(shape_border['ENEMY'][i-1][1])+'),loc_c('+str(shape_border['ENEMY'][i-1][2])+'),loc_d('+str(shape_border['ENEMY'][i-1][3])+'))')
        for i in range(37,40):
            if status[i]!=0:
                f.write(', Shield(id('+ids[i]+'),'+'gid('+ids[i]+'),'+'stat('+str(status[i])+'),loc_a('+str(shape_border['SHIELD'][i-37][0])+'),loc_b('+str(shape_border['SHIELD'][i-37][1])+'),loc_c('+str(shape_border['SHIELD'][i-37][2])+'),loc_d('+str(shape_border['SHIELD'][i-37][3])+'))')
        if status[40]!=0:
            f.write(', Player(id(P1),'+'gid(P1),'+'HP('+str(output['reward'])+'),'+'LIVES('+str(output['lives'])+'),'+'stat('+str(status[40])+'),loc_a('+str(shape_border['PLAYER'][0][0])+'),loc_b('+str(shape_border['PLAYER'][0][1])+'),loc_c('+str(shape_border['PLAYER'][0][2])+'),loc_d('+str(shape_border['PLAYER'][0][3])+'))')
        if status[41]!=0:
            f.write(', Bullet(id(RE1),'+'gid(RE1),'+'stat('+str(status[41])+'),loc_a('+str(shape_border['RE'][0][0])+'),loc_b('+str(shape_border['RE'][0][1])+'),loc_c('+str(shape_border['RE'][0][2])+'),loc_d('+str(shape_border['RE'][0][3])+'))')
        for i in range(42,200):
            if status[i]!=0 and type(status[i][-1])==int:
                f.write(', Bullet(id('+ids[i]+'),'+'gid('+ids[status[i][-1]]+'),RD('+str(DRS[i])+'),loc_a('+str(shape_border['BULLET'][i-42][0])+'),loc_b('+str(shape_border['BULLET'][i-42][1])+'),loc_c('+str(shape_border['BULLET'][i-42][2])+'),loc_d('+str(shape_border['BULLET'][i-42][3])+'))')
        if contact!=[{},{},{},{},{}]:
            for val in contact:
                if val!={}:
                    f.write(', Con(id(c'+str(k)+'),'+'gid('+'c'+str(k)+'),'+'obj1('+ids[val['obj1']]+'),'+'obj2('+ids[val['obj2']]+'))')
        if(actions.has_key(output['action'])):
            print output['action']
            f.write(', '+ actions[output['action']] + '(id(F),gid(F))')
        f.write('\n')
    
    if(frame>100):
        with open('/media/sf_common_file/Atari_online/record/record_8.txt','a+')as f:
            f.write('t('+str(frame)+')')
            if status[40]!=0:
                f.write(', '+ 'Player'+'(id(P1),'+'gid(P1),'+'HP('+str(output['reward'])+'),'+'LIVES('+str(output['lives'])+'),'+'stat('+str(status[40])+'),loc_a('+str(shape_border['PLAYER'][0][0])+'),loc_b('+str(shape_border['PLAYER'][0][1])+'),loc_c('+str(shape_border['PLAYER'][0][2])+'),loc_d('+str(shape_border['PLAYER'][0][3])+'))')
            for i in range(42,55):
                if status[i]!=0 and type(status[i][-1])==int:
                    f.write(', '+ 'Bullet'+'(id('+ids[i]+'),'+'gid('+ids[status[i][-1]]+'),RD('+str(DRS[i])+'),loc_b('+str(shape_border['BULLET'][i-42][1])+'),loc_c('+str(shape_border['BULLET'][i-42][2])+'))')
            if contact!=[{},{},{},{},{}]:
                for val in contact:
                    if val!={}:
                        f.write(', '+'Con'+'(id('+'c'+str(k)+'),'+'gid('+'c'+str(k)+'),'+'obj1('+ids[val['obj1']]+'),'+'obj2('+ids[val['obj2']]+'))')
            if(actions.has_key(output['action'])):
                f.write(', '+ actions[output['action']] + '(id(F),gid(F))')
            f.write('\n')
    '''



       
        

            
            
        
        
