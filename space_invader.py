import time
import sys
import zmq
import json
import cPickle as pickle
from ale_python_interface import ALEInterface
import zerorpc
import copy
from space_invader.get_pixel import border_range,border_pixels,border_all,B_pixel,P_pixel,P_border,RE_border,RE_pixel
from space_invader.contact import find_contact

hp = 0
pre_infor =[]
actions = {'1':'fire','4':'force_left','3':'force_right'}
colors={'ENEMY':[122,95],'SHIELD':[107],'PLAYER':[98],'BULLET':[58,217],'RE':[51,74]}
shape_module = pickle.load(open("./space_invader/shape_module.txt",'r'))
ids= {}
for i in range(1,37):
    ids[i] = 'e'+str(i)
for i in range(37,40):
    ids[i] = 's'+str(i-36)
for i in range(42,200):
    ids[i] = 'b'+str(i-41)
ids[40] = 'p1'
ids[41] = 're1'
init_frame = 200


def send_command():
    real_world=['','']
    c = zerorpc.Client()
    c.connect("tcp://127.0.0.1:4242")
    frame = 0
    action = '0'
    reward = ale.act(action)
    
    f = open('./real_world','w+')
    f.write('')
    f.close()
    f = open('./real_world','a+')
    skip_index = 0
    i =-3
    
    while not ale.game_over():          
        if action == 'EAGAIN':
            break
        if skip_index<150:
            action = '3'
        elif skip_index>250:
            action = '4'
    
        else:
            action = '1'
        reward = ale.act(action)
        screen = ale.getScreenGrayscale()
        #image = ale.saveScreenPNG(str(frame))
        lives = ale.lives()
        if (ale.getFrameNumber() >init_frame and skip_index%2==0):
            frame+=1
            output = {'screen':screen,'reward':reward,'lives':lives,'action':action}
            result = space_invaders(output,frame)
            if(len(result)!=0):
                i+=1
                result = 't('+str(i)+')'+ str(result)
                real_world = [real_world[1],result]
                if(i>-1):    
                    rep={}
                    rep[i]=real_world
                    f.write(result)
        skip_index +=1

def space_invaders(output,frame):
    pixel ={'ENEMY':[],'SHIELD':[],'PLAYER':[],'BULLET':[],'RE':[]}
    status={}
    for i in range(1,500):
        status[i]=0
    contact =[{},{},{},{},{}]
    DRS = {}
    global pre_infor,hp,enemy_exploding_border
    hp += int(output['reward'])
    if frame == 1:
        border ={'ENEMY':[],'SHIELD':[],'PLAYER':[],'BULLET':[],'RE':[]}
        shape_border = border_all(colors,border,output['screen'])
        shape_pixel = border_pixels(colors,shape_border,pixel,output['screen'])
        status_pre = {}
        contact_pre = {}
        for i in range(1,50):
            status_pre[i]=0
    else:
        status_pre = pre_infor[0]
        shape_border_pre = pre_infor[1]
        contact_pre = pre_infor[2]

        shape_border={'ENEMY':[],'SHIELD':[],'PLAYER':[],'BULLET':[],'RE':[]}
        for key,val in shape_border_pre.items():
            if key =='ENEMY':
                for m in val:
                    new=[]
                    #update row range
                    if m!=[]:
                        cur=[]
                        if m[1]-m[0]>9:
                            for i in xrange(max(0,m[0]+10),min(m[0]+25,195)):
                                for j in xrange(max(0,m[2]-2),min(m[3]+3,160)):
                                    if output['screen'][i][j][0] in colors[key]:
                                        cur.append(i)
                            if cur!=[]:                
                                new.append(cur[0])
                                new.append(cur[-1])

                            #update column range
                                cur=[]
                                for i in xrange(max(0,m[2]-2),min(m[3]+3,160)):
                                    for j in xrange(max(0,m[0]+10),min(m[0]+20,195)):
                                        if(output['screen'][j][i][0] in colors[key]):
                                            cur.append(i)
                                new.append(cur[0])
                                new.append(cur[-1])
                            
                        else:
                            for i in xrange(max(0,m[0]-2),min(m[1]+3,195)):
                                for j in xrange(max(0,m[2]-2),min(m[3]+3,160)):
                                    if output['screen'][i][j][0] in colors[key]:
                                        cur.append(i)
                            if cur!=[]:                
                                new.append(cur[0])
                                new.append(cur[-1])

                            #update column range
                                cur=[]
                                for i in xrange(max(0,m[2]-2),min(m[3]+3,160)):
                                    for j in xrange(max(0,m[0]-3),min(m[1]+4,195)):
                                        if output['screen'][j][i][0] in colors[key]:
                                            cur.append(i)
                                new.append(cur[0])
                                new.append(cur[-1])
                    shape_border[key].append(new)
            elif key=='SHIELD':
                for m in val:
                    new=[]
                    if m!=[]:
                        cur=[]
                        for i in xrange(max(0,m[0]-5),min(m[1]+6,194)):
                            for j in xrange(max(0,m[2]-5),min(m[3]+6,160)):
                                if output['screen'][i][j][0] in colors[key]:
                                    cur.append(i)
                        if cur!=[]:                
                            new.append(cur[0])
                            new.append(cur[-1])

                        #update column range
                            cur=[]
                            for i in xrange(max(0,m[2]-5),min(m[3]+6,160)):
                                for j in xrange(max(0,m[0]-5),min(m[1]+6,194)):
                                    if output['screen'][j][i][0] in colors[key]:
                                        cur.append(i)
                            new.append(cur[0])
                            new.append(cur[-1])
                    shape_border[key].append(new)

            elif key=='BULLET':
                for m in val:
                    new =[]
                    #update row range
                    if m!=[]:
                        cur =[]
                        for i in xrange(max(0,m[0]-10),min(m[1]+11,195)):
                            for j in xrange(m[2],m[2]+1):
                                if output['screen'][i][j][0] in colors[key]:
                                    cur.append(i)
                        if cur!=[]:                
                            new.append(cur[0])
                            new.append(cur[-1])
                            new.append(m[-1])
                            new.append(m[-1])
                    shape_border[key].append(new)   
        #updated shape_pixel
        shape_pixel = border_pixels(colors,shape_border,pixel,output['screen'])
        
        # add new bullet into shape_pixel
        bullet_pixel = B_pixel(output['screen'])
        for A in bullet_pixel:
            label =1
            for j in range(0,len(shape_pixel['BULLET'])):
                B = shape_pixel['BULLET'][j]
                if A==B:
                    label=0
                    break
                if any([A==B[i:i+len(A)] for i in range(0,len(B)-len(A)+1)]):
                    shape_pixel['BULLET'][j]=A
                    shape_border['BULLET'][j]=[A[0][0],A[-1][0],A[0][1],A[-1][1]]
                    label=0
                    break
            if label:
                shape_pixel['BULLET'].append(A)
                shape_border['BULLET'].append([A[0][0],A[-1][0],A[0][1],A[-1][1]])
        # add player,RE 
        shape_border['PLAYER'] =P_border(output['screen'])
        shape_pixel['PLAYER'] = P_pixel(output['screen'],shape_border['PLAYER'])
        shape_border['RE'] =RE_border(output['screen'])
        shape_pixel['RE'] = RE_pixel(output['screen'],shape_border['RE'])
        
    shape_pixel_origin = copy.deepcopy(shape_pixel)

    #shape_generalize and recognition
    ID=1
    for val1 in shape_pixel['ENEMY']:
        if val1!=[]:   
            if status_pre[ID]==-1: 
                status[ID]=-1
                shape_border['ENEMY'][ID-1]=shape_border_pre['ENEMY'][ID-1]
            
            else:
                status[ID]=10

            if contact_pre!=[{},{},{},{},{}]:
                for m in contact_pre:
                    if m!={}:
                        if m['obj2']==ID:
                            status[ID]=-1
                            break 

        ID+=1
    ID=37
    for val in shape_pixel['SHIELD']:
        if val !=[]:
            start_x=val[0][0]
            start_y=val[0][1]
            for j in val:
                j[0]-=start_x
                j[1]-=start_y
            if val==shape_module['ST']:
                    status[ID] = 10
            else:
                status[ID]=-1
            if(ID==39):           #eliminating  inteference having same color with sheilds
                break
        ID+=1
    ID=40
    for val in shape_pixel['PLAYER']:
        if val !=[]:
            if status_pre[40]==-1: 
                status[40]=-1
            else:
                status[40]=10
            if contact_pre!=[{},{},{},{},{}]:
                for m in contact_pre:
                    if m!={}:
                        if m['obj2']==40:
                            status[40]=-1
                            break    
    ID=41
    for val in shape_pixel['RE']:
        if val !=[]:
            if status_pre[41]==-1: 
                status[41]=-1
            else:
                status[41]=10
            if contact_pre!=[{},{},{},{},{}]:
                for m in contact_pre:
                    if m!={}:
                        if m['obj2']==41:
                            status[41]=-1
                            break    

    ID=42
    pre = []
    A1=[122,95,107]
    A2=[107,98]
    k=0
    for val in shape_pixel['BULLET']:
        if val==[]:
            ID+=1
            continue
        #elif val==pre:
            #continue
        else: 
            status[ID]=[len(val)]
            status[ID].append([val[0][0],val[-1][0],val[0][1]])
            column = val[0][1]
            if status_pre[ID]!=0:
                if len(status_pre[ID])==3:
                    status[ID].append(status_pre[ID][2])
                elif status_pre[ID][1][0]< val[0][0]:
                    for i in xrange(35,-1,-1):
                         if shape_border['ENEMY'][i]!=[] and val[0][1]>=shape_border['ENEMY'][i][2] and val[0][1]<=shape_border['ENEMY'][i][3]:
                            if status[i+1]!=-1:
                                status[ID].append(i+1)
                                break
                            else:
                                i-=6      
                elif status_pre[ID][1][0] > val[0][0]:
                    status[ID].append(40)
            '''
            #add DRS:relative distance to any entity in front of bullet
            mark = 1
            if len(status[ID])==3:
                if status[ID][2] == 40:
                    for row in range(val[0][0],20,-1):
                        if output['screen'][row][column][0]!=0:
                            DRS[ID] =val[0][0]-row -1
                            mark = 0
                            break
                    if mark:
                        DRS[ID]=-1
                else:
                    for row in range(val[-1][0]+1,195):
                        if output['screen'][row][column][0]!=0:
                            DRS[ID] = row - val[-1][0]-1
                            mark = 0
                            break
                    if mark:
                        DRS[ID]=-1
            '''
            # add DRS:bullet_P and Enemy, bullet and player,perpendicular distance
            mark = 1
            if len(status[ID])==3:
                if status[ID][2] == 40:
                    for row in range(val[0][0],20,-1):
                        #if output['screen'][row][column][0]!=0 and output['screen'][row][column][0]!=107:
                        if output['screen'][row][column][0] in [122,95]:
                            DRS[ID] =val[0][0]-row -1
                            mark = 0
                            break
                    if mark:
                        DRS[ID]=-1
                else:
                    for row in range(val[-1][0]+1,195):
                        # shield normal location:[[157, 174, 42, 49], [157, 174, 74, 81], [157, 174, 106, 113]]
                        if output['screen'][row][column][0]!=0 and row>175:
                            DRS[ID] = row - val[-1][0]-1
                            mark = 0
                            break
                    if mark:
                        DRS[ID]=-1            
            ID+=1
        pre=val
    pre_infor =[status,shape_border,contact]
    #event='t('+str(frame)+')'
    event = ''
    
    if (status[40]==10):

        player_middle_col = shape_border['PLAYER'][0][2] 
        player_middle_row = shape_border['PLAYER'][0][0]
        
        # sort enemy by relative distance between enemy and player
        rd_list = []
        rd_map = {}
        for i in range(1,37):
            if (status[i]==0):
                continue
            rd_cur = (shape_border['ENEMY'][i-1][1]-player_middle_row)*(shape_border['ENEMY'][i-1][1]-player_middle_row)+(shape_border['ENEMY'][i-1][2]-player_middle_col)*(shape_border['ENEMY'][i-1][2]-player_middle_col)
            rd_list.append(rd_cur)
            if rd_cur in rd_map:
                rd_map[rd_cur].append(i)
            else:
                rd_map[rd_cur] = [i]
        rd_list.sort()
        for k in xrange(len(rd_list)):
            for i in rd_map[rd_list[k]]:
                if (player_middle_col in xrange(shape_border['ENEMY'][i-1][2],shape_border['ENEMY'][i-1][3]+1)) or (int(player_middle_col+6) in xrange(shape_border['ENEMY'][i-1][2],shape_border['ENEMY'][i-1][3]+1)):
                    temp = 185 -1 -shape_border['ENEMY'][i-1][1]
                    event+=', '+'enemy'+'(id(e'+str(i)+'),'+'gid(e'+str(i)+'),status('+str(status[i])+'),rd_p('+str(temp)+'),loc_b('+str(shape_border['ENEMY'][i-1][1])+'),loc_c('+str(shape_border['ENEMY'][i-1][2])+'))'
                else:
                    event+=', '+'enemy'+'(id(e'+str(i)+'),'+'gid(e'+str(i)+'),status('+str(status[i])+'),loc_b('+str(shape_border['ENEMY'][i-1][1])+'),loc_c('+str(shape_border['ENEMY'][i-1][2])+'))'    
        event += ', '+ 'player'+'(id(p1),'+'gid(p1),'+'hp('+str(hp)+'),'+'lives('+str(output['lives'])+'),'+'stat('+str(status[40])+'),loc_b('+str(shape_border['PLAYER'][0][1])+'),loc_c('+str(shape_border['PLAYER'][0][2])+'))'
        if status[41]!=0:
            event +=', '+ 'enemy_red'+'(id(re1),gid(re1),'+'status('+str(status[41])+'),'+'loc_b('+str(shape_border['RE'][0][0])+'loc_c('+str(shape_border['RE'][0][2])+'))'

        for i in range(42,42+len(shape_border['BULLET'])):
            contact_id = 0
            if shape_border['BULLET'][i-42]!=[] and len(status[i])==3:
                if (status[i][-1]==40):
                    label = 1
                    for j in xrange(36,0,-1):
                        '''
                        if status[j]==-1:
                            return ''
                        '''
                        if status[j]==10:
                            if(shape_border['BULLET'][i-42][0]>shape_border['ENEMY'][j-1][0] and shape_border['BULLET'][i-42][2] in xrange(shape_border['ENEMY'][j-1][2],shape_border['ENEMY'][j-1][3]+1) ):
                                event += ', '+ 'bullet_p'+'(id('+ids[i]+'),'+'gid(p1),rd_e('+str(DRS[i])+'),rd_eid(e'+str(j)+'),loc_b('+str(shape_border['BULLET'][i-42][1])+'),loc_c('+str(shape_border['BULLET'][i-42][2])+'))' 
                                if(DRS[i]==0 and status[j]==10):
                                    contact[contact_id]['obj1']= i
                                    contact[contact_id]['obj2'] = j
                                    contact_id +=1
                                label = 0
                                break
                    if label:
                        if(status[41]==10):
                            if(shape_border['BULLET'][i-42][0]>shape_border['RE'][0][0] and shape_border['BULLET'][i-42][2] in xrange(shape_border['RE'][0][2],shape_border['RE'][0][3]+1) ):
                                event += ', '+ 'bullet_p'+'(id('+ids[i]+'),'+'gid(p1),rd_e('+str(DRS[i])+'),rd_eid(re1),loc_b('+str(shape_border['BULLET'][i-42][1])+'),loc_c('+str(shape_border['BULLET'][i-42][2])+'))'
                                label = 0
                        if label:
                            event += ', '+ 'bullet_p'+'(id('+ids[i]+'),'+'gid(p1),loc_b('+str(shape_border['BULLET'][i-42][1])+'),loc_c('+str(shape_border['BULLET'][i-42][2])+'))'
                        
                else:
                    if(DRS[i]==0 and status[40]==10):
                        contact[contact_id]['obj1'] = i
                        contact[contact_id]['obj2'] = 40
                        contact_id +=1
                    if(DRS[i] == -1):
                        event += ', '+ 'bullet'+'(id('+ids[i]+'),'+'gid('+ids[status[i][-1]]+'),loc_b('+str(shape_border['BULLET'][i-42][1])+'),loc_c('+str(shape_border['BULLET'][i-42][2])+'))'
                    else:
                        event += ', '+ 'bullet'+'(id('+ids[i]+'),'+'gid('+ids[status[i][-1]]+'),rd('+str(DRS[i])+'),loc_b('+str(shape_border['BULLET'][i-42][1])+'),loc_c('+str(shape_border['BULLET'][i-42][2])+'))'
        if contact!=[{},{},{},{},{}]:
            for k in range(len(contact)):
                if contact[k]!={}:
                    if(contact[k]['obj2'] == 40):
                        event += ', '+'con'+'(id('+'c'+str(k)+'),'+'gid('+'c'+str(k)+'))'
                    elif(contact[k]['obj2']<37 or contact[k]['obj2']==41 ):
                        event += ', '+'con_e'+'(id('+'e'+str(k)+'),'+'gid('+'e'+str(k)+'))'
                    else:
                        event += ', '+'con_s'+'(id('+'s'+str(k)+'),'+'gid('+'s'+str(k)+'))'
        if(actions.has_key(output['action'])):
            event += ', '+ actions[output['action']] + '(id(f),gid(f))'
        event+='\n'
        print shape_border['ENEMY']
        return event
    
    return ''

if __name__ == '__main__':
    
    ale = ALEInterface()
    ale.setBool('display_screen', True)
    ale.loadROM('./ROMS/space_invaders.bin')
    send_command()
