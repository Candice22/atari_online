import cPickle as pickle
import copy
from contact import find_contact
from Get_pixel import border1,border_pixels,BULLET_P,border_pixels_BP
from contain import contain_AB,contain_BA
from contact import find_contact,find_contact1
import time

start = time.clock()
START_time=1
END_time=3
pre_infor = {}
Name = ['player','enemy_w','enemy_b','bullet_p','bullet','truck']
colors={'player':[184,167],'enemy_w':[236,0],'enemy_b':[92,17],'bullet':[184,167],'truck':[0]} #basic color
colors_all=[184,167,236,0,92,17,167,0,131,107,217,150,199]     #all color of objects inside main part except bullet_p
bullet_agent=[]

def chopper_command(output,time):
    border ={'player':[],'enemy_w':[],'enemy_b':[],'bullet_p':[],'bullet':[],'truck':[]}
    pixel ={'player':[],'enemy_w':[],'enemy_b':[],'bullet_p':[],'bullet':[],'truck':[]}
    border_cur={'player':[],'enemy_w':[],'enemy_b':[],'bullet_p':[],'bullet':[],'truck':[]}  #border_cur updated border based on last time, border_new use border1 function to find all object
    pixel_cur={'player':[],'enemy_w':[],'enemy_b':[],'bullet_p':[],'bullet':[],'truck':[]}
    global pre_infor
    status={}
    contact=[]
    #give Id to each object
    if time == START_time:
        border_cur = border1(colors,border,output)
        pixel_cur = border_pixels(colors,border,pixel,output)
        pixel_cur['bullet_p'] = BULLET_P(output,colors_all)
        if pixel['bullet_p']!=[]:
            border['bullet_p'] =[[pixel['bullet_p'][0][0][0],pixel['bullet_p'][0][-1][0],pixel['bullet_p'][0][0][1],pixel['bullet_p'][0][-1][1]]] 
    else:
        filename1= 'chopper/time_status/f_'+str(time-1)+'.txt'
        f = open(filename1,'rb')
        status_pre = pre_infor.status
        border_pre= pre_infor.border
        #update border
        for key,val in border_pre.items():
            if key=='enemy_b':
                for m in val:
                    label=0
                    exploding=[]
                    new=[]
                    if m!=[]:
                        if len(m)==2:
                            m=m[1]
                            for i in xrange(max(56,m[0]),min(m[1]+1,168)):
                                for j in xrange(max(8,m[2]),min(m[3]+1,160)):
                                    if output['screen'][i][j][0]==184 or output['screen'][i][j][0]==199 or output['screen'][i][j][0]==167:
                                        exploding.append(-1)
                                        exploding.append([max(56,m[0]),min(m[1]+1,168),min(m[2],168),min(m[3]+1,160)])
                                        label=1
                                        break
                                if label:
                                    break
                        cur=[]
                        for i in xrange(max(56,m[0]-5),min(m[1]+5+1,168)):
                            for j in xrange(max(8,m[2]-5),min(m[3]+5+1,160)):
                                for c in colors[key]:
                                    if output['screen'][i][j][0]==c:
                                        cur.append(i)
                        if cur!=[]:                
                            new.append(cur[0])
                            new.append(cur[-1])
                        elif exploding==[]:
                            for i in xrange(max(56,m[0]),min(m[1]+1,168)):
                                for j in xrange(max(8,m[2]),min(m[3]+1,160)):
                                    if output['screen'][i][j][0]==184or output['screen'][i][j][0]==199 or output['screen'][i][j][0]==167  :
                                        exploding.append(-1)
                                        exploding.append([max(56,m[0]),min(m[2]+1,168),min(m[1],168),min(m[3]+1,160)])
                                        label=1
                                    break
                                if label:
                                    break
                        #update column range
                        cur=[]
                        for i in xrange(max(8,m[2]-5),min(m[3]+5+1,160)):
                            for j in xrange(max(56,m[0]-5),min(m[1]+5+1,168)):
                                for c in colors[key]:
                                    if output['screen'][j][i][0]==c:
                                        cur.append(i)
                        if cur!=[]:
                            new.append(cur[0])
                            new.append(cur[-1])
                        if exploding!=[]:
                            new=exploding
                    border_cur[key].append(new)
            elif key=='enemy_w':
                for m in val:
                    new=[]
                    label=0
                    exploding=[]
                    if m!=[]:
                        if len(m)==2:
                            m=m[1]
                            for i in xrange(max(56,m[0]),min(m[1]+1,168)):
                                for j in xrange(max(8,m[2]),min(m[3]+1,160)):
                                    if output['screen'][i][j][0]==184 or output['screen'][i][j][0]==199 or output['screen'][i][j][0]==167:
                                        exploding.append(-1)
                                        exploding.append([max(56,m[0]),min(m[1]+1,168),min(m[2],168),min(m[3]+1,160)])
                                        label=1
                                        break
                                if label:
                                    break
                        cur=[]
                        for i in xrange(max(56,m[0]-5),min(m[1]+5+1,168)):
                            for j in xrange(max(8,m[2]-5),min(m[3]+5+1,160)):
                                for c in colors[key]:
                                    if output['screen'][i][j][0]==c or output['screen'][i][j][0]==0:
                                        cur.append(i)
                        if cur!=[]:                
                            new.append(cur[0])
                            new.append(cur[-1])
                        elif exploding==[]:
                            for i in xrange(max(56,m[0]),min(m[1]+1,168)):
                                for j in xrange(max(8,m[2]),min(m[3]+1,160)):
                                    if output['screen'][i][j][0]==184 or output['screen'][i][j][0]==199 or output['screen'][i][j][0]==167:
                                        exploding.append(-1)
                                        exploding.append([max(56,m[0]),min(m[1]+1,168),min(m[2],168),min(m[3]+1,160)])
                                        label=1
                                        break
                                if label:
                                    break
                        #update column range
                        cur=[]
                        for i in xrange(max(8,m[2]-5),min(m[3]+5+1,160)):
                            for j in xrange(max(56,m[0]-5),min(m[1]+5+1,168)):
                                for c in colors[key]:
                                    if output['screen'][j][i][0]==c or output['screen'][j][i][0]==0:
                                        cur.append(i)
                        if cur!=[]:
                            new.append(cur[0])
                            new.append(cur[-1])
                        if exploding!=[]:
                            new=exploding
                    border_cur[key].append(new)
                
            elif key == 'player':
                for m in val:
                    exploding=[]
                    new=[]
                    if m!=[]:
                        if len(m)==2:
                            m=m[1]
                            for i in xrange(max(56,m[0]),min(m[1]+1,168)):
                                for j in xrange(max(8,m[2]),min(m[3]+1,160)):
                                    if output['screen'][i][j][0]==199 :
                                        exploding.append(-1)
                                        exploding.append([max(56,m[0]),min(m[1]+1,168),min(m[2],168),min(m[3]+1,160)])
                                        label=1
                                        break
                                if label:
                                    break
                        cur=[]
                        for i in xrange(max(56,m[0]-2),min(m[1]+2+1,168)):
                            label=1
                            for j in xrange(min(m[2]-2,168),min(m[3]+2+1,160)):
                                for c in colors[key]:
                                    if output['screen'][i][j][0]==c:
                                        cur.append(i)
                                        label=0
                                        break
                            if label and cur!=[]:
                                break
                        if cur!=[]:                
                            new.append(cur[0])
                            new.append(cur[-1])
                        elif exploding==[]:
                            for i in xrange(max(56,m[0]),min(m[1]+1,168)):
                                for j in xrange(max(8,m[2]),min(m[3]+1,160)):
                                    if output['screen'][i][j][0]==199 :
                                        exploding.append(-1)
                                        exploding.append([max(56,m[0]),min(m[1]+1,168),min(m[2],168),min(m[3]+1,160)])
                                        label=1
                                        break
                                if label:
                                    break
                        #update column range
                        cur=[]
                        for i in xrange(max(8,m[2]-2),min(m[3]+2+1,160)):
                            label=1
                            for j in xrange(max(56,m[0]-2),min(m[1]+2+1,168)):
                                for c in colors[key]:
                                    if output['screen'][j][i][0]==c:
                                        cur.append(i)
                                        label=0
                                        break
                            if label and cur!=[]:
                                break                            
                        if cur!=[]:
                            new.append(cur[0])
                            new.append(cur[-1])
                        if exploding!=[]:
                            new=exploding
                    border_cur[key].append(new)
            elif key == 'bullet_p':
                for m in val:
                    new =[]
                    new1=[]
                    if m!=[]:
                        cur =[]
                        for i in xrange(m[-1],8,-1):
                                    if output['screen'][m[0]][i][0] not in colors_all:
                                        cur.append(i)
                                    elif output['screen'][m[0]][i][0]==131:
                                        break
                        if cur!=[]:                
                            new1.append(cur[-1])
                        cur=[]
                        for i in xrange(m[-1],160):
                                    if output['screen'][m[0]][i][0] not in colors_all:
                                        cur.append(i)
                                    elif output['screen'][m[0]][i][0]==131:
                                        break
            
                        if cur!=[]:                
                            new1.append(cur[-1])
                            new.append(m[0])
                            new.append(m[0])
                            new.extend(new1)
                    border_cur[key].append(new)
            elif key=='truck':
                for m in val:
                    new=[]
                    if m!=[]:              
                        #update column range
                        cur=[]
                        for i in xrange(max(8,m[2]-5),min(m[3]+5+1,160)):
                            for j in xrange(max(56,m[0]),min(m[1]+5+1,168)):
                                for c in colors[key]:
                                    if output['screen'][j][i][0]==c or output['screen'][j][i][0]==107:
                                        cur.append(i)
                        if cur!=[]:
                            new.append(m[0])
                            new.append(m[1])
                            new.append(cur[0])
                            new.append(cur[-1])
                    border_cur[key].append(new)
                    
            elif key=='bullet':
                for m in val:
                    new=[]
                    if m!=[]:
                        cur=[]
                        for i in xrange(max(56,m[0]-2),min(m[1]+2+1,168)):
                            label=1
                            for j in xrange(min(m[2]-2,168),min(m[3]+2+1,160)):
                                for c in colors[key]:
                                    if output['screen'][i][j][0]==c:
                                        cur.append(i)
                                        label=0
                                        break
                            if label and cur!=[]:
                                break
                        if cur!=[]:                
                            new.append(cur[0])
                            new.append(cur[-1])
                        #update column range
                        cur=[]
                        for i in xrange(max(8,m[2]-2),min(m[3]+2+1,160)):
                            label=1
                            for j in xrange(max(56,m[0]-2),min(m[1]+2+1,168)):
                                for c in colors[key]:
                                    if output['screen'][j][i][0]==c:
                                        cur.append(i)
                                        label=0
                                        break
                            if label and cur!=[]:
                                break                            
                        if cur!=[]:
                            new.append(cur[0])
                            new.append(cur[-1])
                    border_cur[key].append(new)     
        for i in BULLET_P(output,colors_all):
            border['bullet_p'].append([i[0][0],i[0][0],i[0][1],i[-1][1]])
        border_new = border1(colors,border,output)
        border_new['bullet_p']=border['bullet_p']
        #add new objects
        for key,val in border_new.items():
            if key=='bullet_p':
                for i in val:
                    label=1
                    for j in border_cur[key]:
                        if contain_BA(i,j):
                            label=0
                            break
                    if i not in border_cur[key] and label:
                            border_cur[key].append(i)
            elif key=='player':
                if border_cur['player']==[] and val!=[]:
                    border_cur['player']=[val[0]]
            
            elif key=='bullet':
                if border_cur[key]==[]:
                    border_cur[key]=border_new[key]
                else:
                    cur=[]
                    for i in border_cur[key]:
                        if i==[]:
                            cur.append([])
                            continue
                        for j in val:
                            if contain_AB(j,i):
                                if j not in cur:
                                    cur.append(j)
                                label=0
                                break
                    for j in val:
                        if j not in cur:
                            cur.append(j)
                    border_cur['bullet']=cur
                for i in xrange(len(border_cur[key])):
                    if i>len(bullet_agent)-1:
                        for j in xrange(len(border_cur['enemy_w'])):
                            if len(border_cur['enemy_w'][j])==4 and border_cur['bullet'][i][2]>=border_cur['enemy_w'][j][2] and border_cur['bullet'][i][2]<=border_cur['enemy_w'][j][3]:
                                bullet_agent.append(['enemy_w'+str(j+1)])
                                break
                            elif bullet_agent!=[]:
                                 bullet_agent.append(bullet_agent[-1])
                            continue
                        for j in xrange(len(border_cur['enemy_b'])):
                            if len(border_cur['enemy_b'][j])==4 and border_cur['bullet'][i][2]>=border_cur['enemy_b'][j][2] and border_cur['bullet'][i][2]<=border_cur['enemy_b'][j][3]:
                                bullet_agent.append(['enemy_b'+str(j+1)])
                                break
                            elif bullet_agent!=[]:
                                 bullet_agent.append(bullet_agent[-1])
                            continue
            elif key=='enemy_w':
                if len(border_new['enemy_w'])==2:
                    cur = [[min(border_new['enemy_w'][0][0],border_new['enemy_w'][1][0]),max(border_new['enemy_w'][0][1],border_new['enemy_w'][1][1]),min(border_new['enemy_w'][0][2],border_new['enemy_w'][1][2]),max(border_new['enemy_w'][0][3],border_new['enemy_w'][1][3])]]
                    border_new['enemy_w']=cur
                for i in border_new['enemy_w']:
                    label=1
                    for j in xrange(len(border_cur[key])):
                        if border_cur[key][j]==[]:
                            continue
                        elif contain_BA(i,border_cur[key][j]):
                            label=0
                            break
                        elif contain_AB(i,border_cur[key][j]):
                            border_cur[key][j]=i
                            label=0
                            break
                    if label:
                       border_cur[key].append(i)
                     
            else:    
                for i in val:
                    label=1
                    for j in xrange(len(border_cur[key])):
                        if border_cur[key][j]==[]:
                            continue
                        elif contain_BA(i,border_cur[key][j]):
                            label=0
                            break
                        elif contain_AB(i,border_cur[key][j]):
                            border_cur[key][j]=i
                            label=0
                            break
                    if label:
                       border_cur[key].append(i)

    pixel_cur = border_pixels(colors,border_cur,pixel,output)
    pixel_cur['bullet_p']=border_pixels_BP(colors_all,border_cur['bullet_p'],output)

    for m in Name:
        i = 1
        for l in border_cur[m]:
            s=m+str(i)
            if l==[]:
                status[s]=0
            elif len(l)==2:
                status[s]=-1
            else:
                status[s]=1
            i+=1
    pre_infor['status'] = status
    pre_infor['border'] = border_cur
    # find contact point
    for i in xrange(len(border_cur['bullet'])):
        for j in pixel_cur['player']:
            if border_cur['bullet'][i]!=[]:
                s=find_contact(time,border_cur['bullet'][i],output,pixel_cur['bullet'][i],j,'bullet','player')
                if s!=[]:
                    contact.extend(s)
    for i in xrange(len(border_cur['bullet_p'])):
        for j in pixel_cur['enemy_b']:
            if border_cur['bullet_p'][i]!=[]:
                s=find_contact1(time,border_cur['bullet_p'][i],output,pixel_cur['bullet_p'][i],j,'bullet_p','enemy_b')
                if s!=[]:
                    contact.extend(s)
    for i in xrange(len(border_cur['bullet_p'])):
        for j in pixel_cur['enemy_w']:
            if border_cur['bullet_p'][i]!=[]:
                s=find_contact1(time,border_cur['bullet_p'][i],output,pixel_cur['bullet_p'][i],j,'bullet_p','enemy_w')
                if s!=[]:
                    contact.extend(s)
    for i in xrange(len(border_cur['player'])):
        for j in pixel_cur['enemy_b']:
            if border_cur['player'][i]!=[]:
                s=find_contact(time,border_cur['player'][i],output,pixel_cur['player'][i],j,'player','enemy_b')
                if s!=[]:
                    contact.extend(s)
    for i in xrange(len(border_cur['player'])):
        for j in pixel_cur['enemy_w']:
            if border_cur['player'][i]!=[]:
                s=find_contact(time,border_cur['player'][i],output,pixel_cur['player'][i],j,'player','enemy_w')
                if s!=[]:
                    contact.extend(s)
    event = 't('+str(time)+')'
    for i in Name:
        if i=='bullet':
            for j in xrange(len(border_cur[i])):
                event += ', '+i+'('+ 'id(b'+str(j+1)+'),'+'gid('+str(bullet_agent[j])+'),'+'loc_b('+str(border_cur[i][j][1])+'),loc_c('+str(border_cur[i][j][2])+'),status('+str(status[str(i)+str(j+1)])+')'
        elif i=='bullet_p':
            for j in xrange(len(border_cur[i])):
                event += ', '+i+'('+ 'id(p'+str(j+1)+'),gid(p'+str(j+1)+'),'+'loc_b('+str(border_cur[i][j][1])+'),loc_c('+str(border_cur[i][j][2])+'),status('+str(status[str(i)+str(j+1)])+'),action('+str(output['action'])+'),'+'HP('+str(output['reward'])+'),'+'LIVES('+str(output['lives'])+'))'
        else:
            for j in xrange(len(border_cur[i])):
                if len(border_cur[i][j])==2:
                    event += ', '+i+'('+ 'id('+str(j+1)+'),'+'loc_b('+str(border_cur[i][j][-1][1])+'),loc_c('+str(border_cur[i][j][-1][2])+'),,status('+str(status[str(i)+str(j+1)])+'))'
                else:
                    event += ', '+i+'('+ 'id('+str(j+1)+'),'+'loc_b('+str(border_cur[i][j][1])+'),loc_c('+str(border_cur[i][j][2])+'),status('+str(status[str(i)+str(j+1)])+'))'
    if contact!=[]:
        event += ', '+'con('+'id(c1),gid(c1))'
            
if __name__ == '__main__':
    
    ale = ALEInterface()
    ale.setBool('display_screen', True)
    ale.loadROM('./ROMS/space_invaders.bin')
    send_command()
    




    
