import sys
import zmq
import json
from random import randrange
import zerorpc
import copy
from ale_python_interface import ALEInterface
import seaquest.tools as tools


# basic information

row_limit = [45,153]    #[0,210] by default
col_limit = [0,160]     #[0,160] by default

background_color = [0,32,60]
entity_colors = {
    'player': [172,217],       #53,236,117, #[60,32]
    'swimmer': [85,64,144,141],
    'enemy': [127,147,141,157,197],      #when overlap with swimmer, color is 141 , animating color is 157,197
    'bullet_p': [172,217,144],    #when overlap with swimmer,color is 144
    'bullet':[]
    }
colors_exploding ={
    'player':[236,117],
    }
exploding_pairs ={
    'player': ['enemy','bullet'],
    'enemy': ['bullet_p'],
    }
exploding_pairs_color = {}
for entity in exploding_pairs:
    exploding_pairs_color[entity] = {}
    for i in exploding_pairs[entity]:
        exploding_pairs_color[entity][i] = entity_colors[i]
 
#colors_all = [0,32,60,172,85,64,127,147,217,236,117,53]

init_frame = 150
pre_infor = []
def send_command():
    real_world=['','']
    c = zerorpc.Client()
    c.connect("tcp://127.0.0.1:4242")
    frame = 0
    legal_actions = ale.getLegalActionSet()
    
    f = open('./real_world.txt','w+')
    f.write('')
    f.close()
    f = open('./real_world.txt','a+')
    skip_index = 0
    i =-3
    
    while not ale.game_over():
        action = legal_actions[randrange(len(legal_actions))]
        if action == 'EAGAIN':
            break
        reward = ale.act(action)
        screen = ale.getScreenGrayscale()
        #image = ale.saveScreenPNG(str(frame))
        lives = ale.lives()
        if (ale.getFrameNumber() >init_frame):
            frame+=1
            output = {'screen':screen,'reward':reward,'lives':lives,'action':action}
            result = atari_visual_output(output,frame)
            #result = ''
            if(len(result)!=0):
                i+=1
                result = 't('+str(i)+')'+ str(result)
                real_world = [real_world[1],result]
                if(i>-1):    
                    rep={}
                    rep[i]=real_world
        skip_index +=1
def atari_visual_output(output,frame):
    # initialize variables
    border = {}
    pixel = {}
    RD = {}
    contact = []
    status = {}
    global pre_infor
    
    for entity in entity_colors:
        status[entity] = []
        border[entity] = []
        pixel[entity] = []
        RD[entity] = []

    if frame == 1:
        shape_border = tools.get_all_borders(entity_colors,border,output['screen'])
        shape_pixel = tools.get_pixels(entity_colors,shape_border,pixel,output['screen'])

        for entity in shape_border:
            status[entity] = []
            for i in xrange(len(shape_border[entity])):
                status[entity].append(10)
        contact_pre = []
    else:
        shape_border = border
        for key,val in pre_infor[1].items():
            for m in val:
                new = []
                
                if m!=[]:
                    # update row xrange
                    cur = []
                    for i in xrange(max(0,m[0]-5),min(m[1]+6,row_limit[1])):
                        for j in xrange(max(0,m[2]-5),min(m[3]+6,160)):
                            if output['screen'][i][j][0] in entity_colors[key]:
                                cur.append(i)
                                break
                    if cur!=[]:
                        new.append(cur[0])
                        new.append(cur[-1])
                    # update column xrange
                        cur = []
                        for i in xrange(max(0,m[2]-5),min(m[3]+6,160)):
                            for j in xrange(max(0,m[0]-5),min(m[1]+6,194)):
                                if output['screen'][j][i] in entity_colors[key]:
                                    cur.append(i)
                                    break
                        new.append(cur[0])
                        new.append(cur[-1])
                shape_border[key].append(new)
        # add new entity
        shape_border_new = tools.get_all_borders(entity_colors,border,output['screen'])
        for key,val in shape_border_new.items():     
            for entity_loc in val:
                mark = 1
                for j in xrange(len(shape_border[key])):
                    if tools.is_a_contain_b(shape_border[key][j],entity_loc):
                        mark = 0
                        break
                    elif tools.is_a_contain_b(entity_loc,shape_border[key][j]):
                        shape_border[key][j] = entity_loc
                        mark = 0
                        break
                if mark:
                    shape_border[key].append(entity_loc)

        # update pixels of entity
        shape_pixel = tools.get_pixels(entity_colors,shape_border,pixel,output['screen'])

        # status recognizition
        for entity in shape_border:
            for i in xrange(len(shape_border[entity])):
                if(shape_border[entity][i]==[]):
                    status[entity].append(0)
                elif i<len(pre_infor[0][entity]) and pre_infor[0][entity][i]==-1:
                    status[entity].append(-1)
        for con in pre_infor[2]:
            for key,val in con.items():
                if key in exploding_pairs:
                    for i in exploding_pairs[key]:
                        if i in con:
                            break
                            status[key][val] = -1                
    # detect contact
    contact_temp = []
    for entity in shape_pixel:

        # to find contact between exploding pairs only
        if not entity in exploding_pairs_color:
            continue
        for item in shape_pixel[entity]:
            if len(item) == 0:
                continue
            con = tools.find_contact(item, entity, exploding_pairs_color[entity],output['screen'],background_color)
            if con!= {}:
                contact_temp.append(con)
    filt = []        
    #identify entity id in contact, filter out duplication in contact
    for i in xrange(len(contact_temp)):
        for key,val in contact_temp[i].items():
            for j in xrange(len(shape_pixel[key])):
                if val in shape_pixel[key][j]:
                    contact_temp[i][key] = j
                    break
        if key+str(j) in filt:
            continue
        else:
            filt.append(key+str(j))
            contact.append(contact_temp[i])
    pre_infor = [status,shape_border,contact]
    return ''

if __name__ == '__main__':
    
    ale = ALEInterface()
    ale.setBool('display_screen', True)
    ale.loadROM('./ROMS/seaquest.bin')
    send_command()



